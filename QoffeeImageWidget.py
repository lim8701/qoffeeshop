import platform

from PySide2.QtCore import QObject, Signal, Slot, Property
from PySide2.QtCore import Qt, QTimer, QEvent, QPoint, QPointF, QLineF, QUrl, QRectF, QSize
from PySide2.QtGui import QImage, QScreen, QMouseEvent, QWheelEvent, QHoverEvent, qAlpha, QColor, QKeyEvent
from PySide2.QtQuick import QQuickView, QQuickItem, QSGSimpleTextureNode, QSGNode
from PySide2 import QtQml

import math
import _qapp


_IS_MAC = (platform.system() == 'Darwin')

UP = 11
DOWN = 12
RIGHT = 13
LEFT = 14


class ImageContainer(QQuickItem):
    """
    QML VTK IMAGE CONTAINER ITEM
    """

    _update_request_event = QEvent(QEvent.UpdateRequest)

    mouseDoubleClicked = Signal(object)
    mousePressed = Signal(object)
    mouseReleased = Signal(object)
    keyPressed = Signal(object, object)
    keyReleased = Signal(object, object)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setFlag(QQuickItem.ItemHasContents, True)
        self.img = QImage()
        self.n = None
        self.setAcceptedMouseButtons(Qt.AllButtons)
        self.setAcceptHoverEvents(True)
        self.vtk_obj = None

    @Slot(object)
    @Slot(object, int)
    def set_image(self, img, n=None):
        self.img, self.n = img, n
        self.update()

        '''
        NOTE 
        sendEvent sends the event directly to receiver.
        So problems are occurred when many events are stacked.
        postEvent adds the event to an queue.
        Therfore, postEvent should be used!!!
        '''
        # _qapp.qapp.sendEvent(self.window(), QEvent(QEvent.UpdateRequest))
        _qapp.qapp.postEvent(self.window(), QEvent(QEvent.UpdateRequest), Qt.HighEventPriority)

    def updatePaintNode(self, node, updatePaintNodeData):
        if node is None:
            node = QSGSimpleTextureNode()
            """
            NOTE new node object should be stored.
            """
            self.node = node

        # Image update.
        self.texture = self.window().createTextureFromImage(self.img)
        node.setTexture(self.texture)

        """
        NOTE  sometimes, last column line of vtk_img used to flicker. (vtk 8.1 issue)
              therefore, need to set the SourceRect as below.
        """
        node.setSourceRect(0, 0, int(self.img.width()), int(self.img.height()))
        node.setRect(0, 0, int(self.width()), int(self.height()))
        node.markDirty(QSGNode.DirtyForceUpdate)

        return node

    def set_vtk(self, vtk_obj):
        if self.vtk_obj:
            # disconnect
            self.vtk_obj.sig_rendered.disconnect(self.set_image)
            # delete
            del self.vtk_obj
            self.vtk_obj = None
            # fill screen with black
            del self.img
            self.img = QImage()
            self.img.fill(QColor(0, 0, 0))
            self.update()
            _qapp.qapp.sendEvent(self.window(), self._update_request_event)

        self.vtk_obj = vtk_obj

        if self.vtk_obj:
            self.vtk_obj.sig_rendered.connect(self.set_image)
            # Set init size
            self.vtk_obj.resize(int(self.width()), int(self.height()))

    def geometryChanged(self, newGeom, oldGeom):
        if not self.isVisible():
            return

        super().geometryChanged(newGeom, oldGeom)

        w = newGeom.width()
        h = newGeom.height()
        # o_w = oldGeom.width()
        # o_h = oldGeom.height()

        if self.vtk_obj:
            self.vtk_obj.resize(int(w), int(h))

    def mouseMoveEvent(self, e):
        if self.vtk_obj:
            self.vtk_obj.mouseMoveEvent(e)

    def mousePressEvent(self, e):
        self.mousePressed.emit(e)
        if self.vtk_obj:
            self.vtk_obj.mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        self.mouseReleased.emit(e)
        if self.vtk_obj:
            self.vtk_obj.mouseReleaseEvent(e)

    def mouseDoubleClickEvent(self, e):
        if e.button() == Qt.LeftButton:
            self.mouseDoubleClicked.emit(e)
            # TODO is it necessary?
            # if self.vtk_obj:
            #     self.vtk_obj.mouseDoubleClickEvent(e)

    # property getter
    def GetFullscreenTrigger(self):
        if not hasattr(self, '_fullsize_trigger'):
            self._fullsize_trigger = False
        return self._fullsize_trigger

    # property setter
    def SetFullscreenTrigger(self, fullsize_trigger):
        self._fullsize_trigger = fullsize_trigger
        self.fullscreenTriggerChanged.emit()

    fullscreenTriggerChanged = Signal()
    fullscreenTrigger = Property(object, GetFullscreenTrigger, SetFullscreenTrigger, notify=fullscreenTriggerChanged)

    def hoverMoveEvent(self, e):
        if self.vtk_obj:
            self.vtk_obj.hoverMoveEvent(e)

    def hoverEnterEvent(self, e):
        pass

    def hoverLeaveEvent(self, e):
        pass

    def keyPressEvent(self, e):
        self.keyPressed.emit(e.key(), e.modifiers())
        if self.vtk_obj:
            self.vtk_obj.keyPressEvent(e)

    def keyReleaseEvent(self, e):
        self.keyReleased.emit(e.key(), e.modifiers())
        if self.vtk_obj:
            self.vtk_obj.keyReleaseEvent(e)

    def wheelEvent(self, e):
        if self.vtk_obj:
            self.vtk_obj.wheelEvent(e)
        # self.wheelEventByAngleDelta(e.angleDelta())
        # print("WWWWWW :: ", e)

    def wheelEventByAngleDelta(self, delta):
        d = delta.y()

        if self.vtk_obj:
            self.vtk_obj.istyle.SetMouseWheelMotionFactor(abs(d) / 120)

            if d > 0:
                self.vtk_obj.view._Iren.MouseWheelForwardEvent()
            elif d < 0:
                self.vtk_obj.view._Iren.MouseWheelBackwardEvent()


    def getMsg(self):
        return self._msg

    def setMsg(self, msg):
        self._msg = msg

    msgChanged = Signal(object)
    msg = Property(object, getMsg, setMsg, notify=msgChanged)


class QoffeeQQuickView(QQuickView):

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        # self.screenChanged.connect(self.on_screen_changed)
        # self.setFlags(self.flags() | Qt.FramelessWindowHint)
        self.isFullscreen = False
        self.isScreenChanged = False
        QtQml.qmlRegisterType(ImageContainer, "QoffeeShop", 1, 0, "ImageContainer")
        # QtQml.qmlRegisterType(MaskedMouseArea, "cymaskarea", 1, 0, "MaskedMouseArea")

    def show(self, isMaximize=True):
        """
        Overriden Method

        NOTE Make sure that QQuickView is shown with "_delayed_start()" !!
          Because, Some machines(Windows/Mac) can cause qtquick's renderer problems.
        """

        def _delayed_start():
            super(QQuickView, self).showMaximized() if isMaximize else super(QQuickView, self).show()
        QTimer.singleShot(500, _delayed_start)

        # self.align_center_on_screen()

    def align_center_on_screen(self):
        # align center on screen
        _x = (self.screen().geometry().width()/2) - (self.width()/2)
        _y = (self.screen().geometry().height()/2) - (self.height()/2)
        self.setPosition(_x, _y)

    def initializeTitleBar(self, use_magnet=False):
        self.use_magnet = use_magnet
        self.titleBar = self.rootObject().findChild(QObject, 'titleBar')
        assert self.titleBar
        self.titleBar.sig_close.connect(self.on_close)
        self.titleBar.sig_minimize.connect(self.on_minimize)
        self.titleBar.sig_maximize.connect(self.on_maximize)
        self.titleBar.sig_moved.connect(self.on_titlebar_moved)
        self.titleBar.sig_released.connect(self.on_titlebar_released)

    @Slot()
    def on_close(self):
        self.close()

    @Slot()
    def on_minimize(self):
        self.setWindowState(Qt.WindowMinimized)

    @Slot()
    def on_maximize(self):
        if self.isFullscreen == False:
            self.setWindowState(Qt.WindowFullScreen) if _IS_MAC else self.setWindowState(Qt.WindowMaximized)
            self.isFullscreen = True
        else:
            self.setWindowState(Qt.WindowNoState)
            self.isFullscreen = False

    @Slot(float, float)
    def on_titlebar_moved(self, _x, _y):
        if self.windowState() == Qt.WindowFullScreen or self.windowState() == Qt.WindowMaximized:
            return

        displacement_x = self.framePosition().x() + _x
        displacement_y = self.framePosition().y() + _y
        self.setFramePosition(QPoint(int(displacement_x), int(displacement_y)))

    @Slot(float, float)
    def on_titlebar_released(self, _x, _y):
        if not self.use_magnet:
            return

        # Hot Corner Magnet
        offset = [40, 65] if _IS_MAC else [40, 40]
        if self.framePosition().x() - self.screen().geometry().x() < offset[0] \
                and self.framePosition().y() - self.screen().geometry().y() < offset[1]:
            self.setFramePosition(QPoint(self.screen().geometry().x(), self.screen().geometry().y()))
        elif (self.screen().geometry().x() + self.screen().geometry().width()) - \
                (self.frameGeometry().x() + self.frameGeometry().width()) < offset[0] \
                and self.framePosition().y() - self.screen().geometry().y() < offset[1]:
            new_x = (self.screen().geometry().x() + self.screen().geometry().width()) - self.frameGeometry().width()
            new_y = self.screen().geometry().y()
            new_point = QPoint(new_x, new_y)
            self.setFramePosition(new_point)

    @Slot(QScreen)
    def on_screen_changed(self, screen):
        if self.isScreenChanged == False:
            self.isScreenChanged = True
        else:
            self.isScreenChanged = False

    @Slot(object)
    def repaint_item(self, item):
        """
        NOTE Force update.
        """
        item.update()
        _qapp.qapp.sendEvent(item.window(), QEvent(QEvent.UpdateRequest))


class MAIN_QWIN():
    _win = None
